from random import randint

licencias = list()
def crear_licencia():
	alfanumerico = "0123456789abcdefghi".upper()
	licencia = list()
	for x in range(5):
		grupo = ""
		for y in range(5):
			indice = randint(1,len(alfanumerico))
			#grupo.append(alfanumerico[indice-1])
			grupo = grupo + alfanumerico[indice-1]
		licencia.append(grupo)

	l = "{0}-{1}-{2}-{3}-{4}".format(*licencia)
	l = "-".join(licencia)
	return l

while len(licencias) < 5:
	posible_licencia = crear_licencia()
	if posible_licencia not in licencias: 
		licencias.append(posible_licencia)
		print posible_licencia
 
