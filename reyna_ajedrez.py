"""
mostrar las posiciones de un tablero de ajedrez donde 2
reynas no se pueden matar.
"""

# Generar tablero de 8x8
def generar_tablero():
	tablero = [ range(8) for x in range(8)]
	return tablero
#print tablero

# llenar cada celda con sus coordenadas.
def generar_tablero_coordenadas(vacio=False):
	tablero = generar_tablero()
	for x in range(8):
		for y in range(8):
			if not vacio:
				tablero[x][y] = (x,y)
			else:
				tablero[x][y] = 0
	return tablero

# mostrando tablero con coordenadas
tablero = generar_tablero_coordenadas()
for fila in tablero:
	print fila

def mov_lineal(x,y):
	total = list()
	def mov_horizontal(x,y):
		resultados = list()
		# movimiendo derecha
		for h in range(x+1,7+1):
			resultados.append((h,y))

		# movimiento izquierda
		for h in range(0,x):
			resultados.append((h,y))

		return sorted(resultados,key=lambda x: x[0])

	def mov_vertical(x,y):
		resultados = list()
		# movimiento arriba
		for v in range(y+1,7+1):
			resultados.append((x,v))

		# movimiento abajo
		for v in range(0,y):
			resultados.append((x,v))
		return sorted(resultados, key=lambda x: x[1])

	total = mov_horizontal(x,y)
	total.extend(mov_vertical(x,y))

	return total

def mov_diagonal(x,y):
	total = list()


	#back slash : \
	def mov_backslash(x,y):
		respuesta = list()
		a = x if x > y else y
		# abajo
		for d in range(1,8-a):
			respuesta.append((x+d,y+d))
		
		a = x if x < y else y
		# arriba
		for d in range(1,8-(7-a)):
			respuesta.append((x-d,y-d))

		return respuesta
	
	# slash : /
	def mov_slash(x,y):
		respuesta = list()
		#print "COORDENADAS",x,y
		# hacia abajo
		for i in range(1,8):
			tmp_x,tmp_y = (x+i), (y-i)
			if ( tmp_x < 8) and (0<=tmp_y):
				#print "bucle",tmp_x,tmp_y
				if tmp_x < 8 or tmp_y > 0: 
					respuesta.append((tmp_x,tmp_y))
				else:
					break
			else:
				break

		# hacia arriba
		for i in range(1,8):
			tmp_x,tmp_y = (x-i), (y+i)
			if ( tmp_y < 8) and (0<=tmp_x):
				#print "bucle",tmp_x,tmp_y
				if tmp_y < 8 or tmp_x > 0: 
					respuesta.append((tmp_x,tmp_y))
				else:
					break
			else:
				break

		return respuesta

	total_b = mov_backslash(x,y) # ok
	total_bs = mov_slash(x,y) # verificando
	total = total_b+total_bs
	return total

# ==== TEST ====
x,y = 7,7
print
print "====TEST===="
print "Cordenadas ",x,y
print "Movimiento lineal"
#print mov_horizontal(0,0)
#print mov_vertical(0,0)
print mov_lineal(x,y)
print "Movimiento diagonal"
print mov_diagonal(x,y)

def movimiento_reyna(x,y):
	return mov_lineal(x,y)+mov_diagonal(x,y)


# lista de celdas
celdas_tablero = list()
for fila in tablero:
	celdas_tablero += fila

print len(celdas_tablero)
print celdas_tablero

# pruebas con metodos
def marcar_movimientos(pos_x,pos_y):
	movidas = movimiento_reyna(pos_x,pos_y)
	tablero_vacio = generar_tablero_coordenadas(vacio=True)
	for x in range(8):
		for y in range(8):
			posicion = (x,y)
			if  posicion in movidas:
				tablero_vacio[x][y] = 3
	tablero_vacio[pos_x][pos_y]	= 5		
	print ""
	for fila in tablero_vacio:
		print fila

marcar_movimientos(3,3)


