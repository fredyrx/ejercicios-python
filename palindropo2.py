# -*- coding: utf-8 -*-
from Ayuda import leer
cadena = leer("palabra")
def reemplazar(letra):
	return "f" if letra == "*" else letra
palabra = map(reemplazar,list(cadena))
invertido = list(palabra)
invertido.reverse()
palabra.extend(invertido)
print "PALABRA:",cadena
print "PALINDROPO: ", "".join(palabra)