#
"""
1. equipo que mas goleo a  otro
2. equipo que metio mas goles
3. el equipo puntero
4. el equipo que ha tenido por mas partidos la valla sin vencer

Nota: si se tiene mas de un resultado se colocan en la misma linea
"""
# Leemos los resultados
file = open("futbol.in","r")
contenido = file.read()
partidos = contenido.split("\n")

# Inicializamos las variables
partidos.pop()
equipos = []
tabla = {}

equipos = list(set([partido.split()[0] for partido in partidos]))
puntos = {"v":3,"e":1,"p":0}
formato_resultado = "{0} {1}-{2} {3}"

# Creamos la tabla de equipos con valores iniciales:
for equipo in equipos:
	tabla.update({equipo:
		{
		"puntos":0,
		"gf":0,
		"gc":0,
		"pg":0,
		"pe":0,
		"pp":0,
		"max_goleada":0,
		"partidos":[]
		}
	})
	
# Procesamos los partidos:
print ""
print "RESULTADOS PARTIDOS".center(40,"=")
for partido in partidos:
	items = partido.split(" ")
	local = {"equipo":items[0],"goles":int(items[2]),"resultado":"e"}
	visita = {"equipo":items[1],"goles":int(items[3]),"resultado":"e"}
	# Impresion de resultados
	l = [local["equipo"],local["goles"],visita["goles"],visita["equipo"]]
	x = formato_resultado.format(*l)
	# Impresion de resultados
	print x
	# Procesamos resultados
	if local["goles"] > visita["goles"]:
		local["resultado"] = "v"
		visita["resultado"] = "p"
		tabla[local["equipo"]]["pg"] +=1		
		tabla[visita["equipo"]]["pp"] +=1
		# max goleada
		tabla[local["equipo"]]["max_goleada"] = local["goles"] - visita["goles"]		
	elif local["goles"] < visita["goles"]:
		visita["resultado"] = "v"
		local["resultado"] = "p"
		tabla[visita["equipo"]]["pg"] +=1
		tabla[local["equipo"]]["pp"] +=1
		tabla[visita["equipo"]]["max_goleada"] = visita["goles"] - local["goles"]		

	else: # empate
		tabla[visita["equipo"]]["pe"] +=1
		tabla[local["equipo"]]["pe"] +=1


	# Puntos
	tabla[visita["equipo"]]["puntos"] += puntos[visita["resultado"]]
	tabla[local["equipo"]]["puntos"] += puntos[local["resultado"]]
	# Goles a favor
	tabla[visita["equipo"]]["gf"] += visita["goles"]
	tabla[local["equipo"]]["gf"] += local["goles"]
	# Goles en contra
	tabla[visita["equipo"]]["gc"] += local["goles"]
	tabla[local["equipo"]]["gc"] += visita["goles"]
	


lista_ordenada = sorted(tabla.items(),key=lambda x: x[1]["puntos"], reverse=True)
#print lista_ordenada

# Impresion de tabla de resultados
print ""
print "TABLA DE POSICIONES".center(40,"=")
print "EQUIPOS".ljust(15," "),"PTS  ","V  ","E  ","P  ","GF  ","GC  "
for x in lista_ordenada:
	cast = dict([(key,str(value)) for key,value in x[1].items()])
	#print cast
	print x[0].ljust(16),cast["puntos"].ljust(4),cast["pg"].ljust(3),cast["pe"].ljust(3),cast["pp"].ljust(3),cast["gf"].ljust(4), cast["gc"]

# Resultados
print
print "RESULTADOS DE PREGUNTAS".center(40,"=")
print "\n1. equipo que mas goleo a  otro"
print max(lista_ordenada, key=lambda x: x[1]["max_goleada"])[0]
print "\n2. equipo que metio mas goles"
print max(lista_ordenada, key=lambda x: x[1]["gf"])[0]
print "\n3. el equipo puntero"
print lista_ordenada[0][0]
print "\n4. el equipo que ha tenido por mas partidos la valla sin vencer"
print "?"