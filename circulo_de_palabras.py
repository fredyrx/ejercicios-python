

def formar_circulo(*palabras):
	pendientes = list(palabras)
	circulo = list()

	circulo.append(pendientes[0])
	pendientes.remove(circulo[0])

	tmp = list()
	contador = 0
	try:
		while len(circulo) != len(palabras):
			contador += 1
			posibles = list()
			posibles = filter(lambda x: True if x[0][0]==circulo[-1][-1] else False,pendientes)
			if posibles:
				if len(posibles) == 1: 
					circulo.append(posibles[0])
					pendientes.remove(posibles[0])
				else:
					#print "Este caso tiene mas de una solucion,",posibles
					tmp = posibles
					#print "temporales",tmp
					circulo.append(posibles[0])
					pendientes.remove(posibles[0])
			else:
				#buscamos la palabra en el punto donde se encontro mas de 1 opcion
				palabra_buscada = tmp[0]
				# la borramos de tmp
				tmp.remove(palabra_buscada)
				# buscamos el indice de la palabra buscada en el circulo
				indice = circulo.index(palabra_buscada)
				# movemos las palabras posteriores a esta palabra a pendientes
				circulo_tmp = circulo[0:indice]
				pendientes_tmp=circulo[indice:len(circulo)]

				circulo = circulo_tmp
				pendientes.extend(pendientes_tmp)

				circulo.append(tmp[0])
				pendientes.remove(tmp[0])
		print "FIN DE PROCESAMIENTO: Se encontro solucion en %d vueltas" % contador 
		return circulo
	except KeyboardInterrupt:
		print "SOLUCION TEMPORAL".center(50,"=")
		print circulo

# ============================= INIT ===========================
palabras = ['arbol','nexos','susana','otro','listo','orden']
circulo_palbras = list()

palabra_extremos = {} # letra:{inicio:0,fin:0}
# Verificamos que axista solucion
for palabra in palabras:
	inicio,fin = palabra[0], palabra[-1]

	if inicio in palabra_extremos:
		palabra_extremos[inicio]["inicio"] += 1
	else:
		palabra_extremos[inicio] = {"inicio":1,"fin":0}

	if fin in palabra_extremos:
		palabra_extremos[fin]["fin"] += 1
	else:
		palabra_extremos[fin] = {"inicio":0,"fin":1}

for letra in palabra_extremos:
	if palabra_extremos[letra]["inicio"] != palabra_extremos[letra]["fin"]:
		print "No hay solucion para estas palabras"
		break
else:
	print "Ordenar circularmente a:", palabras
	print "\n Si hay solucion, buscando mejor solucion ...\n"
	solucion = formar_circulo(*palabras)
	print "SOLUCION".center(60,"=")
	print solucion


