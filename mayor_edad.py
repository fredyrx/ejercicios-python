# -*- coding: utf-8 -*-
logeado = False

def solo_logueado(f):
	def aux():
		if logeado:
			f()
		else:
			print "redirigiendo a login."
	return aux

@solo_logueado
def index():
	print "Index: mostrarndo listado ..."

if __name__ == "__main__":

	index()
