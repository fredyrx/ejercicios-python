# -*- coding:utf-8 -*-

logueado = True
def login_required(mensaje="login"):
	def decorador(f):
		def aux():
			if logueado:
				f()
			else:
		#		print "No estas logueado"
				print  "Redirigiendo a %s" % mensaje
		return aux
	return decorador
############ VISTAS ############

#@decorador
@login_required("Iniciar sesion")
def perfil(name="framos",*args, **kwargs):
	print "este es el perfil de %s" % name


def login():
	print "Debes iniciar sesion"


if __name__ == "__main__":
	print "Decorador con @"
	perfil()