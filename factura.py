# -*- coding: utf-8 -*-
"""
empresa receptora
empresa emisora

"""
from datetime import date

class Empresa(object):
	def __init__(self,razon_social,ruc,direccion=None,telefono=None,correo=None):
		self.razon_social = razon_social
		self.ruc = ruc
		self.direccion = direccion
		self.telefono = telefono
		self.correo = correo

class Producto(object):

	def __init__(self,nombre,precio):
		self.nombre = nombre
		self.precio = precio

class Detalle(object):

	productos = list()

	def agregar_producto(self,producto,cantidad):
		item = {
			"descripcion":producto.nombre,
			"precio": float(producto.precio),
			"cantidad":cantidad,
		}
		item["sub_total"] = item["precio"]*item["cantidad"]
		self.productos.append(item)

class Factura(object):
	def __init__(self,emisor,receptor,detalle):
		self.emisor = emisor
		self.receptor = receptor
		self.detalle = detalle
		self.fecha_emision = date.today()

	def calcular_total(self):
		total = 0
		total = sum( [item["sub_total"] for item in self.detalle.productos] )
		return total

	def get_forma(self,ancho=90):
		ancho = ancho
		separador = "+"+"-"*ancho + "+" + "\n" 
		margen = "+"+"="*ancho + "+" + "\n" 

		forma = margen
		forma += "|" + " DE: {0} - RUC: {1}".format(self.emisor.razon_social, self.emisor.ruc).center(ancho) + "|\n"
		forma += "|" + " Dirección: {0}".format(self.emisor.direccion).center(ancho+1) + "|\n"
		forma += "|" + " E-Mail: {0}".format(self.emisor.correo).center(ancho) + "|\n"

		forma += separador
		r = "|" + " Señor(es): {0}".format(self.receptor.razon_social).ljust(ancho+1) + "|\n"
		r += "|" + " RUC: {0} Lima: {1}".format(self.receptor.ruc,self.fecha_emision.strftime("%d de %B del %Y")).ljust(ancho) + "|\n"
		r += "|" + " Dirección: {0}".format(self.receptor.direccion).ljust(ancho+1) + "|\n"
		r += "|" + " E-Mail: {0}".format(self.receptor.direccion).ljust(ancho) + "|\n"
		
		forma += r + separador
		titulo_detalle = "CANT".center(6) + "|" + "DESCRIPCION".center(50) + "|" + "P. UNIT".center(15) + "|" + "VALOR VENTA".center(15)
		forma += "|" + titulo_detalle.ljust(ancho)  + "|\n"
		forma += separador
		detalle = ""
		for item in self.detalle.productos:
			producto = " {cantidad: <5} {descripcion:<50} {precio:<15} {sub_total:<15}".format(**item)
			detalle += "|" + producto.ljust(ancho) + "|\n"
		forma += detalle + separador

		sub_total = self.calcular_total()
		p_sub_total = "|{0:<15}| {1:<15}".format("SUB TOTAL",sub_total)
		forma += "|"+"-"*57 + p_sub_total + "|\n"
		
		igv = 0.18
		p_igv = "|{0:<15}| {1:<15}".format("IGV %",igv)
		forma += "|"+"-"*57 + p_igv + "|\n"
		
		total = sub_total * (igv+1)
		p_total = "|{0:<15}| {1:<15}".format("TOTAL",total)
		forma += "|"+"-"*57 + p_total + "|\n"
		forma += margen
		return forma


	def imprimir(self):
		forma = self.get_forma()
	
		print forma 

# =================== INIT ====================

if __name__ == "__main__":

	emisor_datos = {
	"razon_social": "DECO SYSTEM SAC",
	"ruc": "34534534534",
	"direccion": "Santiago de surco",
	"telefono": "654-3456",
	"correo": "empresa@decoperu.com"
	}

	receptor_datos = {
	"razon_social": "BOTICAS Y SALUD SAC",
	"ruc": "67453345343",
	"direccion": "Calle 1, santiago de surco"
	}
	# empresas
	emisor = Empresa(**emisor_datos)
	receptor = Empresa(**receptor_datos)
	# creamos productos
	teclados = Producto("teclado",50)
	monitor = Producto("monitor",350)
	mouse = Producto("mouse",20)
	celular = Producto("celular",700)
	# creamos detalle
	detalle = Detalle()
	# creamos factura
	factura = Factura(emisor,receptor,detalle)
	# registramos items a detalle
	factura.detalle.agregar_producto(teclados,5)
	factura.detalle.agregar_producto(monitor,4)
	factura.detalle.agregar_producto(mouse,12)
	factura.detalle.agregar_producto(celular,2)

	factura.imprimir()




	